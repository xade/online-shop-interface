import Category from '../components/Category';
import Product from '../components/Product';
import Order from '../components/Order';
import Customer from '../components/Customer';
import Shipper from '../components/Shipper';

export default {
    tabs: [
        { 
            title: "Categories",
            component: Category,
        },
        { 
            title: "Products",
            component: Product,
        }, 
        { 
            title: "Orders",
            component: Order,
        },
        {
            title: "Customers",
            component: Customer,
        },
        {
            title: "Shippers",
            component: Shipper,
        },
    ],
}