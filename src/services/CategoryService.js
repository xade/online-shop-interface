import Api from './Api';

const CATEGORY_URL = '/category';

export default {
    async getAllCategories() {
        const url = `${CATEGORY_URL}/all`;
        const response = await Api.get(url);
        return response.data;
    },

    async getCategoryById(id) {
        const url = `${CATEGORY_URL}/${id}`;
        const response = await Api.get(url);
        return response.data;
    },

    async addCategory(category) {
        const url = `${CATEGORY_URL}/add`;
        const response = await Api.post(url, category);
        return response.data;
    },

    async updateCategory(category) {
        const url = `${CATEGORY_URL}/update/${category.id}`;
        const response = await Api.put(url, category);
        return response.data;
    },

    async deleteCategory(id) {
        const url = `${CATEGORY_URL}/delete/${id}`;
        await Api.delete(url);
    },
}