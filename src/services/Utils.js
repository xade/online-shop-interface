// import * as base64 from "byte-base64"

export default {
    upperCaseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    convertToImage(buffer) {
        var binary = '';
        var bytes = new Uint8Array( buffer );
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return window.btoa(binary);
    },
};