import Api from './Api';

const SHIPPER_URL = '/shipper';

export default {
    async getAllShippers() {
        const url = `${SHIPPER_URL}/all`;
        const response = await Api.get(url);
        return response.data;
    },

    async getShipperById(id) {
        const url = `${SHIPPER_URL}/${id}`;
        const response = await Api.get(url);
        return response.data;
    },

    async addShipper(shipper) {
        const url = `${SHIPPER_URL}/add`;
        const response = await Api.post(url, shipper);
        return response.data;
    },

    async updateShipper(shipper) {
        const url = `${SHIPPER_URL}/update/${shipper.id}`;
        const response = await Api.put(url, shipper);
        return response.data;
    },

    async deleteShipper(id) {
        const url = `${SHIPPER_URL}/delete/${id}`;
        await Api.delete(url);
    },
}