import axios from 'axios';

const BASE_URL = 'http://localhost:8080/api/v1';

export default class Api{
    static async get(url) {
        try {
            const response = await axios.get(`${BASE_URL}${url}`);
            console.log('Success', response);
            return response;
        } catch (error) {
            console.log('Error', error.message)
        }
    }

    static async post(url, body) {
        try {
            const response = await axios.post(`${BASE_URL}${url}`, body);
            console.log('Success', response);
            return response;
        } catch (error) {
            console.log('Error', error.message)
        }
    }

    static async put(url, body) {
        try {
            const response = await axios.put(`${BASE_URL}${url}`, body);
            console.log('Success', response);
            return response;
        } catch (error) {
            console.log('Error', error.message)
        }
    }

    static async delete(url) {
        try {
            const response = await axios.delete(`${BASE_URL}${url}`);
            console.log('Success', response);
            return response;
        } catch (error) {
            console.log('Error', error.message)
        }
    }
}