import Api from './Api';

const ORDER_URL = '/order';

export default {
    async getAllOrders() {
        const url = `${ORDER_URL}/all`;
        const response = await Api.get(url);
        return response.data;
    },

    async getOrdersById(id) {
        const url = `${ORDER_URL}/${id}`;
        const response = await Api.get(url);
        return response.data;
    },

    async addOrder(order) {
        const url = `${ORDER_URL}/add`;
        const response = await Api.post(url, order);
        return response.data;
    },

    async updateOrder(order) {
        const url = `${ORDER_URL}/update/${order.id}`;
        const response = await Api.put(url, order);
        return response.data;
    },

    async deleteOrder(id) {
        const url = `${ORDER_URL}/delete/${id}`;
        await Api.delete(url);
    },
}