import Api from './Api';

const CUSTOMER_URL = '/customer';

export default {
    async getAllCustomers() {
        const url = `${CUSTOMER_URL}/all`;
        const response = await Api.get(url);
        return response.data;
    },

    async getCustomerById(id) {
        const url = `${CUSTOMER_URL}/${id}`;
        const response = await Api.get(url);
        return response.data;
    },

    async addCustomer(customer) {
        const url = `${CUSTOMER_URL}/add`;
        const response = await Api.post(url, customer);
        return response.data;
    },

    async updateCustomer(customer) {
        const url = `${CUSTOMER_URL}/update/${customer.id}`;
        const response = await Api.put(url, customer);
        return response.data;
    },

    async deleteCustomer(id) {
        const url = `${CUSTOMER_URL}/delete/${id}`;
        await Api.delete(url);
    },
}