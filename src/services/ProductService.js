import Api from './Api';

const PRODUCT_URL = '/product';

export default {
    async getAllProducts() {
        const url = `${PRODUCT_URL}/all`;
        const response = await Api.get(url);
        return response.data;
    },

    async getProductById(id) {
        const url = `${PRODUCT_URL}/${id}`;
        const response = await Api.get(url);
        return response.data;
    },

    async addProduct(product) {
        const url = `${PRODUCT_URL}/add`;
        const response = await Api.post(url, product);
        return response.data;
    },

    async updateProduct(product) {
        const url = `${PRODUCT_URL}/update/${product.id}`;
        const response = await Api.put(url, product);
        return response.data;
    },

    async updateProductImage(imageFile, id) {
        const url = `${PRODUCT_URL}/update/image/${id}`;
        console.log(url);
        const formData = new FormData();
        formData.append('imageFile', imageFile);
        const response = await Api.put(url, formData);
        return response.data;
    },

    async deleteProduct(id) {
        const url = `${PRODUCT_URL}/delete/${id}`;
        await Api.delete(url);
    },
}